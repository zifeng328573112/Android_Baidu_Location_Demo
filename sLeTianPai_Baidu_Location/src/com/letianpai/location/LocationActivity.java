package com.letianpai.location;

import org.zsl.android.map.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;

public class LocationActivity extends Activity {
	public MapView mapView = null;
	public BaiduMap baiduMap = null;

	// 定位相关声明
	public LocationClient locationClient = null;
	// 自定义图标
	BitmapDescriptor mCurrentMarker = null;
	boolean isFirstLoc = true;// 是否首次定位

	static BDLocation BD_location = null;

	private LocationBDLocationListener locationListener = new LocationBDLocationListener();

	private Marker mMarkerA;
	// 初始化全局 bitmap 信息，不用时及时 recycle
	private BitmapDescriptor bdA;
	private InfoWindow mInfoWindow;

	private double latitude1;
	private double longitude1;
	private double meLatitude;
	private double meLongitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		initView();
		initOverlay();

	}

	private void initView() {
		// TODO Auto-generated method stub
		// 在使用SDK各组件之前初始化context信息，传入ApplicationContext
		// 注意该方法要再setContentView方法之前实现
		SDKInitializer.initialize(getApplicationContext());
		setContentView(R.layout.main_activity);

		Button locationBt = (Button) findViewById(R.id.location_btn);
		locationBt.setOnClickListener(new LocationOnClickListener('L'));

		mapView = (MapView) this.findViewById(R.id.mapView); // 获取地图控件引用
		baiduMap = mapView.getMap();
		// 开启定位图层
		baiduMap.setMyLocationEnabled(true);
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(14.0f);
		baiduMap.setMapStatus(msu);
		baiduMap.setOnMarkerClickListener(new LocationOnMarkerClickListener());
		bdA = BitmapDescriptorFactory.fromResource(R.drawable.icon_marka);

		locationClient = new LocationClient(getApplicationContext()); // 实例化LocationClient类
		locationClient.registerLocationListener(locationListener); // 注册监听函数
		this.setLocationOption(); // 设置定位参数
		locationClient.start(); // 开始定位
		// baiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL); // 设置为一般地图

		// baiduMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE); //设置为卫星地图
		// baiduMap.setTrafficEnabled(true); //开启交通图

		latitude1 = 31.193012 + 0.010002;
		longitude1 = 121.437873;
	}

	/**
	 * 设置定位参数
	 */
	private void setLocationOption() {
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true); // 打开GPS
		option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll"); // 返回的定位结果是百度经纬度,默认值gcj02
		option.setScanSpan(5000); // 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true); // 返回的定位结果包含地址信息
		option.setNeedDeviceDirect(true); // 返回的定位结果包含手机机头的方向

		locationClient.setLocOption(option);
	}

	class LocationOnClickListener implements OnClickListener {

		char type;

		public LocationOnClickListener(char type) {
			this.type = type;
		}

		@Override
		public void onClick(View v) {
			switch (type) {
			case 'L':
				String addrStr = BD_location.getAddrStr();
				double latitude = BD_location.getLatitude();
				double longitude = BD_location.getLongitude();
				double getShortDistance = ShortDistance.GetShortDistance(latitude, longitude, latitude1, longitude1);
				Toast.makeText(LocationActivity.this, "当前位置:" + addrStr + "  两点之间的距离：" + getShortDistance + "米", Toast.LENGTH_LONG).show();
				break;

			default:
				break;
			}
		}

	}

	class LocationBDLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// TODO Auto-generated method stub
			// map view 销毁后不在处理新接收的位置
			if (location == null || mapView == null)
				return;

			float radius = location.getRadius();
			double latitude = location.getLatitude();// 纬度
			double longitude = location.getLongitude();// 经度

			MyLocationData locData = new MyLocationData.Builder().accuracy(radius)
			// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(latitude).longitude(longitude).build();
			baiduMap.setMyLocationData(locData); // 设置定位数据

			if (isFirstLoc) {
				isFirstLoc = false;

				LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLngZoom(ll, 16); // 设置地图中心点以及缩放级别
				// MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				baiduMap.animateMapStatus(u);
			}

			BD_location = location;
			meLatitude = BD_location.getLatitude();
			meLongitude = BD_location.getLongitude();
			handler.postDelayed(runnable, 5000);// 每两秒执行一次runnable.
		}

	}

	private void initOverlay() {
		LatLng llA = new LatLng(latitude1, longitude1);
		OverlayOptions ooA = new MarkerOptions().position(llA).icon(bdA).zIndex(9);
		mMarkerA = (Marker) (baiduMap.addOverlay(ooA));
	}

	class LocationOnMarkerClickListener implements OnMarkerClickListener {

		@Override
		public boolean onMarkerClick(final Marker latlng) {
			Button button = new Button(getApplicationContext());
			button.setBackgroundResource(R.drawable.popup);
			OnInfoWindowClickListener listener = null;
			LatLng position = latlng.getPosition();
			if (latlng == mMarkerA) {
				Log.i("TAG", "点击事件" + latlng);
				button.setText("当前位置：" + "东经" + position.longitude + " 北纬" + position.latitude);
				listener = new OnInfoWindowClickListener() {
					public void onInfoWindowClick() {
						baiduMap.hideInfoWindow();
					}
				};
			}
			LatLng ll = latlng.getPosition();
			mInfoWindow = new InfoWindow(BitmapDescriptorFactory.fromView(button), ll, listener);
			baiduMap.showInfoWindow(mInfoWindow);

			return true;
		}

	}

	Handler handler = new Handler();
	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			// TODO Auto-generated method stub
			// 要做的事情
			repetitionLocation(meLatitude, meLongitude, latitude1, longitude1);
		}
	};

	/**
	 * 
	 * @param mLatitude
	 *            我的纬度
	 * @param mLongitude
	 *            我的经度
	 * @param fLatitude
	 *            好友纬度
	 * @param fLongitude
	 *            好友经度
	 */
	private void repetitionLocation(double mLatitude, double mLongitude, double fLatitude, double fLongitude) {

		double getShortDistance = ShortDistance.GetShortDistance(mLatitude, mLongitude, fLatitude, fLongitude);
		if (getShortDistance > 500) {
			Toast.makeText(LocationActivity.this, "接孩子啊！！！", Toast.LENGTH_LONG).show();
			handler.removeCallbacks(runnable);
		} else {
			Toast.makeText(LocationActivity.this, "5秒后两点之间的距离：" + getShortDistance + "米", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		mapView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mapView.onPause();
	}

	// 三个状态实现地图生命周期管理
	@Override
	protected void onDestroy() {
		// 退出时销毁定位
		locationClient.stop();
		baiduMap.setMyLocationEnabled(false);
		super.onDestroy();
		mapView.onDestroy();
		mapView = null;
		bdA.recycle();
	}
}
